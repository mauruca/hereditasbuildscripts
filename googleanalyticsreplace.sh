#!/bin/sh
# Copyright (C) 2015-2016 Mauricio Costa Pinheiro - Todos os direitos reservados.
# Você pode usar, copiar e distribuir este código sobre os termos da licença.

if [ -z "$1" ]
  then
    echo "Informar o path do build"
    exit 1
fi

if [ -z "$2" ]
  then
    echo "Informar o nome do build"
    exit 1
fi

if [ "$2" = "hereditas" ]
then
  sed -i s/">>changethisgoogleanalyticspropertyid<<"/""/g ./$1/databaseaws.py
fi

if [ "$2" = "test" ]
then
  sed -i s/">>changethisgoogleanalyticspropertyid<<"/""/g ./$1/databaseaws.py
fi

if [ "$2" = "demo" ]
then
  sed -i s/">>changethisgoogleanalyticspropertyid<<"/"UA-76362785-2"/g ./$1/databaseaws.py
fi

if [ "$2" = "engineti" ]
then
  sed -i s/">>changethisgoogleanalyticspropertyid<<"/"UA-76362785-4"/g ./$1/databaseaws.py
fi
