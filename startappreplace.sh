#!/bin/sh
# Copyright (C) 2015-2016 Mauricio Costa Pinheiro - Todos os direitos reservados.
# Você pode usar, copiar e distribuir este código sobre os termos da licença.

if [ -z "$1" ]
  then
    echo "Informar o nome da aplicacao."
    exit 1
fi

# Altera o nome do bando de dados no caso de iniciar um novo
sed -i s/">>hereditasstartapp<<"/"$1"/g ./build/startup.sh
