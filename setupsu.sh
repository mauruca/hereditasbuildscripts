#!/bin/sh
# Copyright (C) 2015 Mauricio Costa Pinheiro - Todos os direitos reservados.
# Você pode usar, copiar e distribuir este código sobre os termos da licença.
#
# Você deve ter recebido uma cópia da licença que está no arquivo LICENCA ou LICENSE. Se não,
# entrar em contato escrevendo para mpinheiro@pobox.com ou mauricio.pinheiro@gmail.com ou mauricio@ur2.com.br.

echo "Criando o super usuário"
echo "from django.contrib.auth.models import User; User.objects.create_superuser('hadmin', 'admin@hereditas.net.br', 'mudar123')" | python3 manage.py shell