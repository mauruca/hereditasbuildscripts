#!/bin/bash
# Copyright (C) 2015 Mauricio Costa Pinheiro - Todos os direitos reservados.
# Você pode usar, copiar e distribuir este código sobre os termos da licença.
#
# Você deve ter recebido uma cópia da licença que está no arquivo LICENCA ou LICENSE. Se não,
# entrar em contato escrevendo para mpinheiro@pobox.com ou mauricio.pinheiro@gmail.com ou mauricio@ur2.com.br.

if [ -z "$1" ]
  then
    echo "Informar: 1 - Execução normal ## 0 - Nova instalação ## 2 - Reset migrations"
    exit 1
fi

echo "Starting Hereditas IO..."

# Se parametro == 0 cria banco de dados
if [ "$1" = "0" ] && [ -f setupdb.sh ]
  then
    echo " "
    echo "Creating database..."
    ./setupdb.sh
fi

echo "var $1"

if [ "$1" = "3" ]; then
    echo " "
    echo "Custom migrations..."
    python3 manage.py makemigrations --no-input hereditascore
    python3 manage.py migrate --fake --no-input hereditascore
fi

if [ "$1" = "2" ]; then
    echo " "
    echo "Reseting migrations..."
    ./clearmigrationsdb.sh
    python3 manage.py makemigrations --no-input hereditascore
    python3 manage.py migrate --fake-initial --no-input hereditascore
fi

if [ "$1" = "1" ]; then
    echo " "
    echo "Running migrations..."
    python3 manage.py makemigrations --no-input
    python3 manage.py migrate --no-input
fi

# Se parametro == 0 cria administrador
if [ "$1" = "0" ] && [ -f setupsu.sh ]
  then
    echo " "
    echo "Creating database schema, data and administrator..."
    python3 manage.py makemigrations --no-input
    python3 manage.py migrate --no-input
    ./setupsu.sh
fi

# Remove arquivo de configuração do administrador
echo " "
echo "Removing setup scripts..."
if [ -f setupdb.sh ]
  then
    rm setupdb.sh
fi
if [ -f setupsu.sh ]
  then
    rm setupsu.sh
fi
if [ -f clearmigrationsdb.sh ]
  then
    rm clearmigrationsdb.sh
fi

echo " "
echo "Running server..."
gunicorn hereditasio.wsgi:application -t 120 -w 2 -b 0.0.0.0:8000
