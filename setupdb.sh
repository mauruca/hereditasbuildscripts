#!/bin/sh
# Copyright (C) 2015-2016 Mauricio Costa Pinheiro - Todos os direitos reservados.
# Você pode usar, copiar e distribuir este código sobre os termos da licença.
#
# Você deve ter recebido uma cópia da licença que está no arquivo LICENCA ou LICENSE. Se não,
# entrar em contato escrevendo para mpinheiro@pobox.com ou mauricio.pinheiro@gmail.com ou mauricio@ur2.com.br.

#echo "CREATE DATABASE <database> WITH ENCODING='UTF8' OWNER=<user> CONNECTION LIMIT=-1;" | psql -h <server> -p <port> -U <user> postgres

echo "Criando o banco de dados"
echo "CREATE DATABASE >>changethisdatabasename<< WITH ENCODING='UTF8' OWNER=>>changethisdatabaseuser<< CONNECTION LIMIT=-1;" |  python3 manage.py dbshell --database=dbadmshell
