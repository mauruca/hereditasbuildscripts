#!/bin/sh
# Copyright (C) 2015-2016 Mauricio Costa Pinheiro - Todos os direitos reservados.
# Você pode usar, copiar e distribuir este código sobre os termos da licença.

if [ -z "$1" ]
  then
    echo "Informar o path do build"
    exit 1
fi

if [ -z "$2" ]
  then
    echo "Informar o nome da base de dados."
    exit 1
fi
if [ -z "$3" ]
  then
    echo "Informar o nome do usuario da base de dados."
    exit 1
fi

if [ -z "$4" ]
  then
    echo "Informar a senha do usuario da base de dados."
    exit 1
fi

if [ -z "$5" ]
  then
    echo "Informar o servidor da base de dados."
    exit 1
fi

if [ -z "$6" ]
  then
    echo "Informar a tipo de http."
    exit 1
fi

if [ -z "$7" ]
  then
    echo "Informar secret key."
    exit 1
fi

databasename=$2
databaseuser=$3
databasepassword=$4
databaseserver=$5
httpparams=$6
skeyparams=$7

if [ "$databasename" = "engineti" ]
then
  databasename="engine"
fi

if [ "$databasename" = "test" ]
then
  databasename="deploytest"
fi

sed -i s/">>changethishttp<<"/"$httpparams"/g ./$1/databasepgaws.py
sed -i s/">>changethiskey<<"/"$skeyparams"/g ./$1/databasepgaws.py
sed -i s/">>changethisdatabasename<<"/"$databasename"/g ./$1/databasepgaws.py
sed -i s/">>changethisdatabaseuser<<"/"$databaseuser"/g ./$1/databasepgaws.py
sed -i s/">>changethisdatabasepassword<<"/"$databasepassword"/g ./$1/databasepgaws.py
sed -i s/">>changethisdatabaseserver<<"/"$databaseserver"/g ./$1/databasepgaws.py

# Altera o nome do bando de dados no caso de iniciar um novo
sed -i s/">>changethisdatabasename<<"/"$databasename"/g ./build/setupdb.sh
sed -i s/">>changethisdatabaseuser<<"/"$databaseuser"/g ./build/setupdb.sh
